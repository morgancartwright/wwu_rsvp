<?php
/**
 * @file
 * wwu_rsvp.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function wwu_rsvp_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_seats_remaining'
  $field_bases['field_seats_remaining'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_seats_remaining',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_rsvp_link'
  $field_bases['field_rsvp_link'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_rsvp_link',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'viewfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'viewfield',
  );

  return $field_bases;
}
