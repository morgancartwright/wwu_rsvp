# WWU Event RSVP System + Pieces

Provides webform validation rules and exported views, rules, and fields for an
event RSVP system.

## Instructions for setting up an RSVP System:

1. Create a content type for the entities you want users to RSVP for with the
   machine name "event". The included feature will attach a field for the number
   of seats and a viewfield that generates a link to the RSVP form for a
   particular event.
2. Create a webform that contains a hidden component with default value
   [current-page:query:event\_id] (this holds the value from the url generated
   by events' viewfields) and an integer field that holds the number of seats
   that the user wants to reserve, as well as any other components you need.
3. Enable the included module and feature.
4. At the RSVP form, add three validation rules:
    - "Numeric values" for the hidden nid field
    - "Require hidden field" for the hidden nid field
    - "Compare to node fields" for the hidden nid field and integer seats field,
      with the configuration "= entity\_id >= field\_remaining\_seats"
5. Edit the rule provided by the feature so that the condition refers to your
   RSVP form and update the tokens in the rules actions to refer to the correct
   components.
6. Edit the view "Event Info for RSVP Form" to link to your calendar when there
   are no results and include the event date when there are not. Add this panel
   pane to the Webform's page.
