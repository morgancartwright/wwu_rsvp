<?php
/**
 * @file
 * wwu_rsvp.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wwu_rsvp_default_rules_configuration() {
  $items = array();
  $items['rules_decrement_seats_after_successful_rsvp'] = entity_import('rules_config', '{ "rules_decrement_seats_after_successful_rsvp" : {
      "LABEL" : "Decrement Seats after Successful RSVP",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "webform_rules", "rules" ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "data_nid" : {
            "webform" : [ "data" ],
            "nid" : { "value" : { "webform-client-form-7230" : "webform-client-form-7230" } }
          }
        }
      ],
      "DO" : [
        { "data_convert" : {
            "USING" : { "type" : "integer", "value" : [ "data:7230--1:0" ] },
            "PROVIDE" : { "conversion_result" : { "node_id" : "Node ID" } }
          }
        },
        { "data_convert" : {
            "USING" : { "type" : "integer", "value" : [ "data:7230--3:0" ] },
            "PROVIDE" : { "conversion_result" : { "reserved_seats" : "Reserved Seats" } }
          }
        },
        { "entity_fetch" : {
            "USING" : { "type" : "node", "id" : [ "node_id" ] },
            "PROVIDE" : { "entity_fetched" : { "event_node" : "Event Node" } }
          }
        },
        { "component_rules_decrement_seats_of_event" : {
            "event_node" : [ "event_node" ],
            "reserved_seats" : [ "reserved_seats" ]
          }
        }
      ]
    }
  }');
  $items['rules_decrement_seats_of_event'] = entity_import('rules_config', '{ "rules_decrement_seats_of_event" : {
      "LABEL" : "Decrement Seats of Event",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "event_node" : { "label" : "Event Node", "type" : "node" },
        "reserved_seats" : { "label" : "Reserved Seats", "type" : "integer" }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "event_node" ],
            "type" : "node",
            "bundle" : { "value" : { "event" : "event" } }
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "event-node:field-remaining-seats" ],
              "op" : "-",
              "input_2" : [ "reserved-seats" ]
            },
            "PROVIDE" : { "result" : { "new_num_seats" : "New Number of Seats" } }
          }
        },
        { "data_set" : {
            "data" : [ "event-node:field-remaining-seats" ],
            "value" : [ "new_num_seats" ]
          }
        }
      ]
    }
  }');
  return $items;
}
